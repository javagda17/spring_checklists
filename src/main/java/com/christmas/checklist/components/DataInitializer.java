package com.christmas.checklist.components;

import com.christmas.checklist.model.User;
import com.christmas.checklist.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {
    // klasa której zadaniem jest załadować się na początku pracy
    // serwisu i dodać użytkownika admin - jeśli takiego nie ma.

    @Autowired
    private UserRepository userRepository;
    // potrzebujemy user repository żeby dodać do bazy użytkownika.

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        Optional<User> optionalUser = userRepository.findByUsername("admin");
        if (!optionalUser.isPresent()) {
            // jeśli użytkownika nie ma, to musimy go dodać do bazy
            User user = new User();
            user.setUsername("admin");
            user.setAdmin(true);
            // do bazy wpisujemy zaszyfrowane hasło
            user.setPassword(bCryptPasswordEncoder.encode("admin"));

            userRepository.save(user);
        }
    }


}

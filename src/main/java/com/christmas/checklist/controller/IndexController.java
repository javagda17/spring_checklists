package com.christmas.checklist.controller;

import com.christmas.checklist.model.User;
import com.christmas.checklist.model.dto.UserRegisterRequest;
import com.christmas.checklist.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @GetMapping("/register")
    // przekażemy model, żeby zaraz przekazać wiadomość do widoku
    public String getRegisterForm(Model model,
                                  @RequestParam(name = "error", required = false) String error) {
        if (error != null) {
            model.addAttribute("message", error);
        }
        return "register";
    }

    @PostMapping("/register")
    public String submitRegisterForm(UserRegisterRequest userRegisterRequest) {
        boolean success = userService.tryRegister(userRegisterRequest);
        if (success) {
            return "redirect:/login";
        }
        return "redirect:/register?error=Registration error";
    }
}

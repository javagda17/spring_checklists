package com.christmas.checklist.controller;

import com.christmas.checklist.model.dto.AddChecklistDto;
import com.christmas.checklist.services.ChecklistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/checklist")
public class ChecklistController {

    @Autowired
    private ChecklistService checklistService;

    @GetMapping("/checkListForm")
    public String getChecklistForm() {
        return "checklistForm";
    }

    @PostMapping("/checkListForm")
    public String submitChecklistForm(AddChecklistDto dto) {

        return "redirect:/checklist/list";
    }

    @GetMapping("/list")
    public String getCheckListLists(Model model){
        return "checklists";
    }

}

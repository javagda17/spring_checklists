package com.christmas.checklist.services;

import com.christmas.checklist.model.User;
import com.christmas.checklist.model.dto.UserRegisterRequest;
import com.christmas.checklist.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public boolean tryRegister(UserRegisterRequest userRegisterRequest) {
        if (userRegisterRequest.getPassword().equals(userRegisterRequest.getPasswordConfirm())) {
            // hasła wpisane w formularzu rejestracji się zgadzają:
            Optional<User> userOptional = userRepository.findByUsername(userRegisterRequest.getUsername());
            if (!userOptional.isPresent()) {
                // jeśli nie udało się znaleźć usera, to możemy się zarejestrować
                User user = new User();
                user.setAdmin(false);
                user.setUsername(userRegisterRequest.getUsername());
                user.setPassword(bCryptPasswordEncoder.encode(userRegisterRequest.getPassword())); // szyfrujemy hasło!

                userRepository.save(user);
                return true; // udało się!
            }
        }
        // we wszystkich pozostałych przypadkach zwracamy brak sukcesu operacji ponieważ nie udało się zarejestrować
        // (zwróć uwagę że nie zwracamy informacji co jest przyczyną błędu rejestracji.
        return false;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}

package com.christmas.checklist.services;

import com.christmas.checklist.model.User;
import com.christmas.checklist.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserLoginService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    // robiłem commita 10 min temu
    // musimy stworzyć service który implementuje UserDetailsService.
    // ta klasa odpowiada za szukanie użytkownika w bazie i zwrócenie go
    // do modułu który odpowiada za logowanie
    @Override
    public UserDetails loadUserByUsername(String usernameFromForm) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByUsername(usernameFromForm);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            // zauważcie że nie weryfikuje hasła - to nie my za to odpowiadamy.
            // nowo stworzony użytkownik będzie miał hasło zweryfikowane dalej - przez spring'a
            String[] roles;
            if(user.isAdmin()) {
                roles = new String[]{"USER", "ADMIN"};
            }else{
                roles = new String[]{"USER"};
            }

            // zwracamy do modułu użytkownika,
            // ale nie użytkownika naszej klasy User, tylko klasy spring'owej
            // korzystamy do tego z builder'a
            return org.springframework.security.core.userdetails.User.builder()
                    .username(user.getUsername())
                    .roles(roles)             // <- musimy użytkownikowi nadać jakąś rolę.
                    .password(user.getPassword())
                    .accountExpired(false)
                    .accountLocked(false)
                    .build();
        }
        // w dokumentacji metody napisane jest, że nie możemy zwracać
        // z tej metody nulla, a jeśli nie znajdzie się użytkownik, to musimy rzucić exception:
        throw new UsernameNotFoundException("Couldn't find user in db.");
    }
}

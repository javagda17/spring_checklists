package com.christmas.checklist.repository;

import com.christmas.checklist.model.CheckListItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CheckListItemRepository extends JpaRepository<CheckListItem, Long> {
}

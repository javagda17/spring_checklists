package com.christmas.checklist.model;

public enum ItemStatus {
    TO_DO, DONE, IN_PROGRESS
}

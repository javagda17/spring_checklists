package com.christmas.checklist.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CheckListItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long itemId;

    @ManyToOne(fetch = FetchType.EAGER)
    private CheckList checklist;

    private ItemStatus itemStatus;

    @Column(unique = true)
    private String name;

    private LocalDateTime itemDateCompleted;

    @Length(max = 300)
    private String content;


}